const inputs = document.querySelectorAll(".controls input");

function handleUodate() {
    const suffix = this.dataset.sizing || "";
    document.documentElement.style.setProperty(`--${this.name}`, this.value + suffix);
}

inputs.forEach(input => input.addEventListener("change", handleUodate));
inputs.forEach(input => input.addEventListener("mousemove", handleUodate));
